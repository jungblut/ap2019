var startposition = 55;
var startSpeed = 5;
// Happy smiley variables
var happyX = 55;
var happyY = 55;
var happySpeed = 7;
// Sad smiley variables
var sadX = 55;
var sadY = happyY+105;
var sadSpeed = 3;
// Disgusted smiley variables
var disgustedX = startposition;
var disgustedY = sadY+105;
var disgustedSpeed = 4;
// Angry smiley variables
var angryX = startposition;
var angryY = disgustedY+105;
var angrySpeed = 8;
// Affraid smiley variables
var afraidX = startposition;
var afraidY = angryY+105;
var afraidSpeed= 1;
// Surprised smiley variables
var surprisedX = startposition;
var surprisedY = afraidY+105;
var surprisedSpeed = 5;


// Create the canvas
function setup() {
  createCanvas(windowWidth,650);
}


// Drawing loop
function draw() {
  // Start every frame with white background
  background(255);
  // Glad smiley
  //Face
  push();
  noStroke();
  fill(0,255,0);
  ellipse(happyX,happyY,100,100);
  pop();
  //Eyes
  fill(0);
  ellipse(happyX+20,happyY-10,15,20);
  ellipse(happyX-20,happyY-10,15,20);
  //Mouth
  noFill();
  strokeWeight(5);
  arc(happyX,happyY+12,50,50,0,PI);
  // Reverse speed if position reaches canvas width
  if(happyX > width-50){
    happySpeed = -happySpeed;
  }
  if(happyX < 50){
    happySpeed = -happySpeed;
  }
  // Set new position before next draw loop
  happyX = happyX + happySpeed;


  // Sad smiley
  //Face
  push();
  fill(0,0,255);
  noStroke();
  ellipse(sadX,sadY,100,100);
  pop();
  //Eyes
  fill(0);
  ellipse(sadX-20,sadY-8,15,20);
  ellipse(sadX+20,sadY-8,15,20);
  //Eyebrows
  line(sadX-35,sadY-20,sadX-15,sadY-30);
  line(sadX+35,sadY-20,sadX+15,sadY-30);
  //Tear
  triangle(sadX+25,sadY+10,sadX+24,sadY+12,sadX+26,sadY+12);
  ellipse(sadX+25,sadY+14,4.5,4.5);
  //Mouth
  noFill();
  strokeWeight(5);
  arc(sadX,sadY+30,50,20,PI,TWO_PI);

  if(sadX > width-50){
    sadSpeed = -sadSpeed;
  }
  if(sadX < 50){
    sadSpeed = -sadSpeed;
  }
  sadX = sadX + sadSpeed;


  // Disgusted smiley
  //Face
  push();
  noStroke();
  fill(98,23,160);
  ellipse(disgustedX,disgustedY,100,100);
  pop();
  //Eyes
  fill(0);
  ellipse(disgustedX-20,disgustedY-10,15,20);
  ellipse(disgustedX+20,disgustedY-10,15,20);
  //Mouth
  noFill();
  strokeWeight(5);
  line(disgustedX-30,disgustedY+15,disgustedX-15,disgustedY+30);
  line(disgustedX,disgustedY+15,disgustedX-15,disgustedY+30);
  line(disgustedX,disgustedY+15,disgustedX+15,disgustedY+30);
  line(disgustedX+30,disgustedY+15,disgustedX+15,disgustedY+30);

  if(disgustedX > width-50){
    disgustedSpeed = -disgustedSpeed;
  }
  if(disgustedX < 50){
    disgustedSpeed = -disgustedSpeed;
  }
  disgustedX = disgustedX + disgustedSpeed;

  // Angry smiley
  //Face
  push();
  fill(255,0,0);
  noStroke();
  ellipse(angryX,angryY,100,100);
  pop();
  //Eyes
  fill(0);
  ellipse(angryX-20,angryY-8,15,20);
  ellipse(angryX+20,angryY-8,15,20);
  //Mouth
  noFill();
  strokeWeight(5);
  arc(angryX,angryY+22,50,5,PI,TWO_PI);

  line(angryX-7,angryY-20,angryX-25,angryY-30);
  line(angryX+7,angryY-20,angryX+25,angryY-30);

  if(angryX > width-50){
    angrySpeed = -angrySpeed;
  }
  if(angryX < 50){
    angrySpeed = -angrySpeed;
  }
  angryX = angryX + angrySpeed;

  // Afraid smiley
  //Smiley face
  push();
  noStroke();
  fill(244,120,26);
  ellipse(afraidX,afraidY,100,100);
  pop();
  fill(0,0,255,75);
  push();
  noStroke();
  arc(afraidX,afraidY,100,100,PI,TWO_PI)
  pop();
  //Eyes
  fill(0);
  ellipse(afraidX-20,afraidY-8,15,20);
  ellipse(afraidX+20,afraidY-8,15,20);
  //Eyebrow
  strokeWeight(5);
  line(afraidX-35,afraidY-23,afraidX-15,afraidY-28);
  line(afraidX+35,afraidY-23,afraidX+15,afraidY-28);
  //Mouth
  strokeWeight(5);
  line(afraidX-30,afraidY+20,afraidX-15,afraidY+25);
  line(afraidX,afraidY+20,afraidX-15,afraidY+25);
  line(afraidX+30,afraidY+20,afraidX+15,afraidY+25);
  line(afraidX,afraidY+20,afraidX+15,afraidY+25);

  if(afraidX > width-50){
    afraidSpeed = -afraidSpeed;
  }
  if(afraidX < 50){
    afraidSpeed = -afraidSpeed;
  }
  afraidX = afraidX + afraidSpeed;

  // Surprised smiley
  //Smiley face
  push();
  fill(255,255,0);
  noStroke();
  ellipse(surprisedX,surprisedY,100,100);
  pop();
  //Eyes
  fill(0);
  ellipse(surprisedX-20,surprisedY-10,15,20);
  ellipse(surprisedX+20,surprisedY-10,15,20);
  //Mouth
  ellipse(surprisedX,surprisedY+20,15,22);

  if(surprisedX > width-50){
    surprisedSpeed = -surprisedSpeed;
  }
  if(surprisedX < 50){
    surprisedSpeed = -surprisedSpeed;
  }
  surprisedX = surprisedX + surprisedSpeed;
}
