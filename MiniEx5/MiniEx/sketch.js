

class Smiley {
  constructor(xpos,ypos,xspeed,yspeed,color, mood){
    this.pos = new createVector(xpos,ypos);
    this.xspeed = xspeed;
    this.yspeed = yspeed;
    this.size = 100;
    this.color = color;
    this.mood = mood;

  }
  show(){
    //Face
    fill(this.color);
    ellipse(this.pos.x, this.pos.y, this.size);
    //Eyes
    fill(0);
    ellipse(this.pos.x+20, this.pos.y-10, 15, 20);
    ellipse(this.pos.x-20, this.pos.y-10, 15, 20);
    //Draw mouth according to mood
    if (this.mood == 'happy') {
      push();
      noFill();
      strokeWeight(4);
      stroke(0);
      arc(this.pos.x, this.pos.y+12, 50, 50, 0, PI);
      pop();
    } else if (this.mood == 'sad'){
      push();
      noFill();
      strokeWeight(4);
      stroke(0);
      arc(this.pos.x, this.pos.y+30,50,20,PI,TWO_PI);
      pop();
      // If no mood is set
    } else if (!this.mood){
      push();
      strokeWeight(4);
      stroke(0);
      line(this.pos.x-20, this.pos.y+20, this.pos.x+20, this.pos.y+20);
      pop();
    }
  }
  move(){
    if(this.pos.x > width-50){
      this.xspeed = -this.xspeed
    }
    if(this.pos.x < 50) {
      this.xspeed = -this.xspeed
    }
    this.pos.x = this.pos.x + this.xspeed

    if(this.pos.y > height-50){
      this.yspeed = -this.yspeed
    }
    if(this.pos.y < 50) {
      this.yspeed = -this.yspeed
    }
    this.pos.y = this.pos.y + this.yspeed

  }
}

function setup(){
  createCanvas(windowWidth, windowHeight);
  sadSmiley = new Smiley (random(50, 550), random(50, 550), 5, 5, color(0, 0, 255), 'sad');
  happySmiley = new Smiley (random(50, 550), random(50, 550), 7, 7, color(255, 255, 0), 'happy')
  neutralSmiley = new Smiley (random(50, 550), random(50, 550), 2, 2, color(0, 255, 0))
}

function draw(){
  background(255);
  noStroke();
  sadSmiley.show();
  happySmiley.show();
  neutralSmiley.show();
  sadSmiley.move();
  happySmiley.move();
  neutralSmiley.move();

}
