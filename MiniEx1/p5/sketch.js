function setup() {
  createCanvas(650,650);
  background(255,255,255);

}
// put setup code here



function draw() {
  // put drawing code here
  fill(0,0,0);
  textSize(16);
  text("Press mouse to reset\nPress S to save",20,20);
  fill(0,random(255),random(255));
  ellipse(mouseX,mouseY,50,50);
  ellipse(mouseY,mouseX,50,50);
  rotate(10);
  frameRate(10);
  if (mouseIsPressed) {
    clear();
  }
}
function keyTyped(){
  if (key === "s"){
    saveCanvas("circleart");
  }
}
