# My Code - cicleartwork
## Screenshot
![screenshot](circleart.png)

## Link
https://cdn.staticaly.com/gl/ejknutsson/ap2019/raw/master/MiniEx1/p5/index.html

## Description
I wanted to make a code that could be used for something, since my knowledge of what is possible in p5.js is still small I made a simple drawing program. First I created a canvas and made the background white:
```
  createCanvas(650,650);
  background(255,255,255);
```
Then I decided on how you can draw. You draw with a circle by moving the mouse, but at the same time there is a circle that reacts opposite to the movement of the mouse. I did this by defining two ellipses in the following way:
```
  ellipse(mouseX,mouseY,50,50);
  ellipse(mouseY,mouseX,50,50);
```
The first ellipse will move with the mouse, having the position mouseX and mouseY. The second ellipse will move opposite the mouse having the opposite position, mouseY and mouseX. Both ellipses will be circles since their height and width are the same.

To make the drawing random and new every time, the color will change in every frame.
```
  fill(random(0),random(255),random(255));
```
The circles will be a color between blue and green in every frame, since it is only green and blue that is defined. Red is set to 0.

I wanted there to be a possibility of resetting the canvas, so I added a if statement.
```
  if (mouseIsPressed) {
    clear();
```
It is very simple, if the mouse is pressed the canvas will be cleared.

I also wanted there to be an option to save ones artwork, so I added to following line:
```
function keyTyped(){
  if (key === "s"){
    saveCanvas("circleart");
```
If S is pressed the canvas will be saved down on the computer with the name "circleart"

To make the user aware of the reset and saving option, I added to lines of text:
```
  fill(0,0,0);
  textSize(16);
  text("Press mouse to reset\nPress S to save",20,20);
```
The text will be black and in size 16, it will read "Press mouse to reset" on the first line and "Press S to save" on the second line, and be placed at 20,20 pixels on the canvas.

# Coding vs reading and writing text
Theres a lot of similarities in writing/reading text and code. We use the same words for both things. Although coding is a language in it self it hasn't come up with new words, its still familiar english words that are used. (are all coding languages in english?)
As I see it the biggest difference between the two things are the rules. Both in writen language and coding language theres a lot of rules. There is gramma and stylistic rules you have to follow. If you don't use correct gramma in english (or danish) it will still be readable, it may be harder to read. But you can read it.
If you don't follow the rules while coding, the code will not be able to run, you have to follow ALL the rules to get the result you desire.
There is also a big difference in the fact that most coding language uses libraries. It is enough to say
```
  rectangle()
```
when using the p5.js library, you only have to use one word to make the code understanderble. But if I want my boyfriend to buy me flowers it would not be enough just to say "flowers". With libraries some words get a predifined meaning making the code more highlevel. We don't do the same with the writen language.

Programming practice to me, is to follow the stilistic and syntax rules, but at the same time look for a  way to break these rules, if it could mean a better result.
