

class Ball {
  constructor(xspeed, yspeed, xpos, ypos, size){
    this.xspeed = xspeed;
    this.yspeed = yspeed;
    this.pos = new createVector(xpos,ypos);
    this.size = size;
    this.visible = true;
  }

  move (){
    this.pos.x += this.xspeed;
    this.pos.y += this.yspeed;
  }

  grow (){
    this.size = this.size + 50;
    return this.size;
  }

  show(){
    if (this.visible) {
      push();
      noStroke();
      fill(255, 0, 0);
      ellipse(this.pos.x, this.pos.y, this.size);
      pop();
    }
  }
}

class Text{
  constructor(textSize){
    this.textSize = textSize;
    this.textvisible = false;
}
show (){
  if(this.textvisible) {
    fill(0);
    textSize(this.textSize);
    textAlign(CENTER, CENTER);
    text("BANG", width/2, height/2);
  }
}
}
// The main code for the program
let bigBall;
let smallBalls = [];
let bang

function setup() {
  createCanvas(600,600);
  bigBall = new Ball (0, 0, width/2, height/2, 50);
  bang = new Text(100);
  frameRate(10);
}

function draw () {
  background(255);
  bigBall.show();
  bang.show();

  for (let i = 0; i < smallBalls.length; i++){
    smallBalls[i].move();
    smallBalls[i].show();
  }
}

function mousePressed() {
  if(bigBall.grow() > width){
    bigBall.visible = false;
    bang.textvisible = true;
    for (let i = 0; i < 50; i++){
      smallBalls[i] = new Ball (int(random(-20,20)), int(random(-20,20)), width/2, height/2, 20);
    }
  }
}
