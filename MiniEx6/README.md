## MiniEx6

# Screenshot

![Bang](Bang1.png)
![BangBang](Bang2.png)
![BangBangBang](Bang3.png)


# Link
https://cdn.staticaly.com/gl/ejknutsson/ap2019/raw/master/MiniEx6/MiniEx/index.html

# Thoughts on the program
I really wanted to make a game for this weeks MiniEx, but found classes and object a litlle hard to work with, so I just concentrated on making something that woked.
I have made on class that is a red ball and two object the big ball that gets bigger when you click and the small balls that starts in the middle and move away from that point.
I find it very practical that you easily can make a class and make different object out of the class with different properties. 
It made tho code much more simple and manegeble, when I had wrapped my head around what it can be used for. I will definatly always consider if I can use a clase to create an object instead of creating many different object, in the future. 

The question of wheter we should put things (and people) in boxes and thereby creating a hieriachy is a bit tricky. I can see why it could be harmfull to put expecialy people in a box. And also objects, when you first have "sorted" it/them you might forget that they also have other properties than just the one box you put them in. 
But on the other hand it is easier to use things if you know what they can do. I don't think it is all bad to dfine and box stuff, but I think we should consider the consequenses of it.