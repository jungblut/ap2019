# MiniEx 10
## Flowcharts
### Individuel Flowcharts
![Bubble flowchart](Flowchart.png)

[Link to MiniEx6](https://gitlab.com/jungblut/ap2019/tree/master/MiniEx6)

My flowchart is from the classes and object week. I think it was fairly easy to draw, seeing some of the others flowchart mine was maybe not as technical. But it explanes well what happens. Although it actually doens't explane the whole program. Since I expect that people stop pressing after the ball goes bang. But infact the small balls keeps erupting every time you click after the big ball has gone.
As it was not supposed to be like that, I left it out of the flowchart.
### Group flowchart

#### Idea one
![Idea1](Idea1.png)
We want to create a map of Aarhus that will stimulate more than just your eys. Connected in a pattern dots will be spread out on the screan and when you press one, you'll get a picture and sound from a place in Aarhus
This idea requieres a JSON file with a picture and a sound linked together, we have earlier had problems with sound as a part of a JSON file, so that might be a problem. Our collection of data might also become a problem, since wee need a lot of data to make a nice map, but we also have to think about peoples anonymity.

#### Idea two
![Idea1](Idea2.png)
We want to challenge peoples prejudice. By showen two (or more) different statements or qualities, the user will decide who they want to start a new society with. When they  hav choosen between eg doctor and gabage man, they will be shown the rest of qualities that person have. And maybe they will be surprised.
We talked about creating a list of all the people that where choosen, but don't know how to do it, so that will be a challenge. We are also a bit unsure if the cases should be real people or personas we make up.

#### Difference in flowchart
I don't think there are that much of a difference in all three cases there are a bigger focus on the concept than the technical stuff. (Eg we could have startet the idea flowchart with "load JSON file") I think it is good to make a flowchart before you start a problem, to get a sense of what you want the program to be able to do. Especially when you are more people working on the flowchart. So you are all on the same page when it comes to execution.

#### Algorithms in our society
 Algorithms are very simply put a set of rules or instructions, for the computer to follow these instruction you have to give them the right input. We also have a lot of rules in our society, especially when it comes to the way we behave around each other. You could say that they our human Algorithms and like computer Algorithmsyou have to give the right input to get the output you expect. Like a computer we where not born with these sets of rules but they where "programmed" into us during our upbringing and sometimes we have a hard time communitcated with each other be course we don't have the same Algorithms in our code.
