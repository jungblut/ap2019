# MiniEx 7

## RunMe:
[https://cdn.staticaly.com/gl/Kragh/ap2019/raw/master/miniExes/MiniEx7/index.html]

## Code
Since I created the code together with Aia and she has uploaded the code, I am only going to link to it.

[https://gitlab.com/Kragh/ap2019/tree/master/miniExes/MiniEx7]

## Screenshot

![Dots and lines](Dots and lines.png)

## ReadMe
This time Aia and I created the code together, we were in doubt about what to do, so we ended up starting with another code and changed that and implemented some rules.
We wanted to make dots and connect lines to them.
We choose to both have an object consisting of invisible dots with lines between them and another one that was visible dots. This illustrates have many ways there is to place both the dots and the lines.
There are different rules in our code, one of them being the area the lines can move in. One is constrained to a smaller area  and the other can move freely on the entire canvas and beyond.
This in our opinion creates a more interesting art work, and you could experience with more boxes and/or more lines.

I think that all code is basically about creating rules. What will happen if you move the mouse or if you click a button? That is determinded with a rule in your code.
Using a if(); and else(); statement or a for(); or while(); loop is you defining what should happen in a certain situation, creating a rule for that situation.
Where it gets really interesting is when you use those rules to create something not thinking about its funktion but for it to be art. Maybe displaying some sort of messege. 
Langdons ant is an example of how you with two simple rules can create something interesting and mesmerizing. Even though it creates the same pattern each time, it is so complicated that it would be hard (and atleast take a VERY long time, to do what the computer does in seconds) for a human to recreate it.
If you really want a piece that is imposible to recreate you can use the noise(); or random(); functions creating something new everytime. The same rules can be interpreted in many different ways by a human and in even more ways by a computer.
The random(); function is one of the functions I've used from day one and in  nearly all my MiniEx, I like the idea of something being unpredicteble and new everytime.
We humans have so much control over everything and it is nice sometime to see what happens when we loose control.



