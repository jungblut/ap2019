// Button positioning distance
let pos = 1440/9;

// DOM variables
let buttonBlack;
let buttonWhite;
let buttonGreen;
let buttonRed;
let buttonYellow;
let buttonBlue;
let buttonOrange;
let buttonPurple;

// Url and API variables
let url;
let api = "https://pixabay.com/api/?key";
let apiKey = "=12103784-c04457569d30421c94d229577&q=";
let q = "color";
let type = "&image_type=photo";
let colors = "&colors=";
let word = ["black", "white", "green", "red", "Yellow", "blue", "orange", "lilac"]; // Categories in the API database



function setup() {
  noCanvas();

  // Creating the black button
  buttonBlack = createButton("");
  buttonBlack.size(pos,100);
  buttonBlack.position(0, 0);
  buttonBlack.style("background-color", "#000000");

  // Creating the white button
  buttonWhite = createButton("");
  buttonWhite.size(pos,100);
  buttonWhite.position(pos, 0);
  buttonWhite.style("background-color", "#ffffff");

  // Creating the green button
  buttonGreen = createButton("");
  buttonGreen.size(pos,100);
  buttonGreen.position(pos*4, 0);
  buttonGreen.style("background-color", "#A1CE5E");

  // Creating the red button
  buttonRed = createButton("");
  buttonRed.size(pos,100);
  buttonRed.position(pos*7, 0);
  buttonRed.style("background-color", "#DD3232");

  // Creating the yellow button
  buttonYellow = createButton("");
  buttonYellow.size(pos,100);
  buttonYellow.position(pos*5, 0);
  buttonYellow.style("background-color", "#FFEF6C");

  // Creating the blue button
  buttonBlue = createButton("");
  buttonBlue.size(pos,100);
  buttonBlue.position(pos*3, 0);
  buttonBlue.style("background-color", "#0065A9");

  // Creating the orange button
  buttonOrange = createButton("");
  buttonOrange.size(pos,100);
  buttonOrange.position(pos*6, 0);
  buttonOrange.style("background-color", "#FBAF5F");

  // Creating the purple button
  buttonPurple = createButton("");
  buttonPurple.size(pos,100);
  buttonPurple.position(pos*2, 0);
  buttonPurple.style("background-color", "#985396");



  // Running color selcting functions
  buttonBlack.mouseClicked(colorSelectBlack);
  buttonWhite.mouseClicked(colorSelectWhite);
  buttonGreen.mouseClicked(colorSelectGreen);
  buttonRed.mouseClicked(colorSelectRed);
  buttonYellow.mouseClicked(colorSelectYellow);
  buttonBlue.mouseClicked(colorSelectBlue);
  buttonOrange.mouseClicked(colorSelectOrange);
  buttonPurple.mouseClicked(colorSelectPurple);
}



// Functions for creating different URLs for each corresponding color
function colorSelectBlack() {
  let url = api + apiKey + q + type + colors + word[0];
  loadJSON(url, gotData);
}

function colorSelectWhite() {
  let url = api + apiKey + q + type + colors + word[1];
  loadJSON(url, gotData);
}

function colorSelectGreen() {
  let url = api + apiKey + q + type + colors + word[2];
  loadJSON(url, gotData);
}

function colorSelectRed() {
  let url = api + apiKey + q + type + colors + word[3];
  loadJSON(url, gotData);
}

function colorSelectYellow() {
  let url = api + apiKey + q + type + colors + word[4];
  loadJSON(url, gotData);
}

function colorSelectBlue() {
  let url = api + apiKey + q + type + colors + word[5];
  loadJSON(url, gotData);
}

function colorSelectOrange() {
  let url = api + apiKey + q + type + colors + word[6];
  loadJSON(url, gotData);
}

function colorSelectPurple() {
  let url = api + apiKey + q + type + colors + word[7];
  loadJSON(url, gotData);
}



// Function for creating the images
function gotData(photo) {
  let img = createImg(photo.hits[int(random(20))].largeImageURL);
  img.position(0, 100);
}
