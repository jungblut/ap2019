let rSlider, gSlider, bSlider;
let mic, face;


function setup() {

  mic = new p5.AudioIn();
  mic.start();
  // Web cam setup
  let capture = createCapture(VIDEO);
  capture.size(640,400);
  capture.position(0,0);

  // create canvas
  var c = createCanvas(710, 600);
  c.position(0,0);
  textSize(15);
  noStroke();


  // create sliders
  rSlider = createSlider(0,255,0); // (min value, max value, start value)
  rSlider.position(60, 520);
  gSlider = createSlider(0, 255, 0);
  gSlider.position(60, 550);
  bSlider = createSlider(0, 255, 0);
  bSlider.position(60, 580);


  //Face tracker setup
  face = new clm.tracker();
  face.init();
  face.start(capture.elt);
}

function draw() {

  let vol = mic.getLevel();
  let volM = int(map(vol, 0, 1, 1, 50));
  let positions = face.getCurrentPosition();

    if (positions.length) {
      ellipse(positions[62][0], positions[62][1], volM, volM);
    }

  const r = rSlider.value();
  const g = gSlider.value();
  const b = bSlider.value();
  fill(r,g,b);
  rect(200,520,80,80);

  push();
  fill(0);
  text('Red', 10 , 535);
  text('Green', 10 , 565);
  text('Blue', 10 , 595);
  text("Press C to clear\nPress S to save", 10, 475)
  pop();
}

function keyTyped() {
  if (key === "c"){
    clear();
  }
  if (key === "s"){
    saveCanvas("SelfieArt");
  }
}
