//3 varibles are defined before the code
var cycles = 8;
var layer = 30;
var color;


function setup() {
 createCanvas(600, 600);
 background(10);
 frameRate (10); // Decides how fast the throbber spins
 color = [color(21,213,255),color(21,213,159),color(21,111,159),color(147,111,159),color(147,204,159),color(247,204,159)];
} // an array of different colors



function draw() {
  fill(10,80);
  noStroke();
  rect(0, 0, width, height); // The fading effect is created with a rectangle with a low alpha, so it becomes seethrough
  drawThrobber(10, 0); //(num,y) Num decides how many circles there is in the throbber, while y, is the position on the y grid
  throbbers(); // The throbbers that come after the initial, increasing their y-position depending on the frameCount
  shapes();
}

function drawThrobber(num, y) {
  push();
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);  //Difining how .
  rotate(radians(cir));
  noStroke();
  fill(247,53,159);
  ellipse(35,y,22,22);


  pop();
}

function throbbers() {
  if (frameCount > cycles) {
    drawThrobber(10, layer);
  }

  if (frameCount > cycles*2) {
    drawThrobber(10, layer*2);
  }

  if (frameCount > cycles*3 ) {
    drawThrobber(10, layer*3);
  }

  if (frameCount > cycles*4){
    drawThrobber(10, layer*4);
  }

  if (frameCount > cycles*5) {
    drawThrobber(10, layer*5);
  }

  if (frameCount > cycles*6 ) {
    drawThrobber(10, layer*6);
  }

  if (frameCount > cycles*7){
    drawThrobber(10, layer*7);
  }
  if (frameCount > cycles*8) {
    drawThrobber(10, layer*8);
  }

  if (frameCount > cycles*9) {
    drawThrobber(10, layer*9);
  }

  if (frameCount > cycles*10 ) {
    drawThrobber(10, layer*10);
  }

  if (frameCount > cycles*11){
    drawThrobber(10, layer*11);
  }
  if (frameCount > cycles*12 ) {
    drawThrobber(10, layer*12);
  }

  if (frameCount > cycles*13){
    drawThrobber(10, layer*13);
  }
}

function shapes() {
  if(frameCount > cycles*14){
    shape1();
}
  if(frameCount > cycles*15){
    shape2();
  }

  if(frameCount > cycles*16){
    shape3();
}
  if(frameCount > cycles*17){
    shape4();
  }

  if(frameCount > cycles*18){
    shape5();
  }
}

function shape1() {

  beginShape();
  fill(random(color));
  vertex(0,600);
  vertex(0,450);
  vertex(320,350);
  vertex(315,600);
  endShape(CLOSE);
}

function shape2() {
  beginShape();
  fill(random(color));
  vertex(0,0);
  vertex(0,420);
  vertex(260,330);
  vertex(20,0);
  endShape(CLOSE);

}
function shape3() {
  beginShape();
  fill(random(color));
  vertex(60,0);
  vertex(450,0);
  vertex(255,270);
  endShape(CLOSE);
}

function shape4(){
  beginShape();
  fill(random(color));
  vertex(495,00);
  vertex(600,0);
  vertex(600,340);
  vertex(320,250);
  endShape(CLOSE);
}

function shape5(){
  beginShape();
  fill(random(color));
  vertex(600,380);
  vertex(600,600);
  vertex(355,600);
  vertex(355,300);
  endShape(CLOSE);

}
