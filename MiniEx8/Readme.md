# MiniEx8 readme

## Code
https://gitlab.com/KristianKruse/ap2019/tree/master/mini_ex08/Genesis

## RunMe

https://cdn.staticaly.com/gl/KristianKruse/ap2019/raw/master/mini_ex08/Genesis/index.html

## Screenshot

![Once upon a time....](Fairytale.png)

## Thoughts
We (Kristian and I) wanted to mix two atypical texts together. We quicly decided on something from the bible, but had a harder time deciding on the other text. 
I the end we went with genre instead of a specifik text. We looked at the first nine bible verse and group the nouns into three groups Person (GOD), place and object.
We wanted to make a .JSON file with the groups, but we didn't manege to make it work. Instead we made an array. I think that in our case, where it's just an array and not more things grouped together (eg a person with an age and haircolor) It makes as much sense to make an array as an .JSON file. 
But I wish we had manage to do it with the .JSON file, becourse I really want to learn how to do that!
We choose some of the nouns in the text and made varibles with them, the varibles was one of the arrays we made, so it was quick to change the words, and the bibleverse stands in it original form in our code.
We used ```'+ Light +'``` in the text, so that the words was seen as code and not  just text. And then  ```light = randomObjects[int(random(4))];``` to replace the word light with a random word from our object array. 
If we had had more time we would have wanted to add a button in some fairytale shape (maeby a rainbow or a wand) and a plin pling sound when it was pressed, refreshing the page and creating a new fairytale.

I think that code poetry is very interesting, both in the program and in the code. The idea of making art with code is pleasing it shows how you can use things in another way than what they where ment to be. p5 is a good example of this. 
Things evolving into something new, but also becoming easier to acces for everyone. Adding poetry you take it to a new level, here you have to understand or at least think about what is happening. And takes away the commeness of again.
We wanted to make a statement about religion with our piece. Mixing up the bible with fairytale word to symbolise that the bible (at least to us) is nothing more than a fairytale. 
We talked about reversing it putting bible words into fairytales, agian to make it clear that such and old book shouldn't be giving more worth than a fairytale. 
If we should have mede it more of a statement, we could have chosen more text from different religions. 

I hope I do not offend anyone with this program and readme, this is reflecting my oppinion and you of course are fully entitled to have a different oppinion. 
